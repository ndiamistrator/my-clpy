from setuptools import setup
from clpy import __version__

setup(
    name='clpy',
    version=__version__,
    author='ndiamistrator',
    author_email='ndiamistrator@gmail.com',
    url='https://gitlab.com/ndiamistrator/my-clpy',
    download_url='https://gitlab.com/ndiamistrator/my-clpy.git',
    py_modules=['clpy'],
)
