#!/bin/env -S python3 -I

# ndiamistrator -- 2022-06-18

'''
    Command line helper
'''

__version__ = '0.0.1'

__all__ = 'SubImporter', 'Importer', 'main', 'main_wrapper'


import sys, builtins


_getitem = dict.__getitem__

class Importer(dict):
    def __getitem__(S, item):
        try:
            return _getitem(S, item)
        except KeyError:
            pass
        if item == '_':
            S[item] = R = getattr(__import__(f'.{item}'), item)
        else:
            module = __import__(item)
            S[item] = R = SubImporter(module.__dict__, [item])
        return R


_new = dict.__new__
_init = dict.__init__

class SubImporter(dict):
    def __new__(C, data, path):
        return _new(C, data)

    def __init__(S, data, path):
        _init(S, data)
        S['__path'] = path

    def __getattribute__(S, attr):
        try:
            return S[attr]
        except KeyError:
            pass
        path = [*S['__path'], attr]
        module = __import__('.'.join(path))
        for subpath in path[1:]:
            module = getattr(module, subpath)
        S[attr] = R = SubImporter(module.__dict__, path)
        return R


def main(argv=None, ns=None):
    if argv is None:
        argv = sys.argv[1:]
    if ns is None:
        ns = dict(builtins.__dict__)

    script = []
    while argv:
        if argv[0] == '--':
            del argv [0]
            break
        script.append(argv[0])
        del argv[0]
    code = compile('\n'.join(script), '<argv>', 'exec')

    ns['argv'] = argv
    exec(code, Importer(ns))


def main_wrapper(*args, **kwargs):
    try:
        main(*args, **kwargs)
    except KeyboardInterrupt:
        print('\nKeyboardInterrupt', file=sys.stderr)
        sys.exit(1)
    except BrokenPipeError:
        print('BrokenPipeError', file=sys.stderr)
        sys.exit(2)


if __name__ == '__main__':
    main_wrapper()
